# Instrucciones taller GIT

## Instalar GIT
En Windows dirijirse a:
https://git-scm.com/downloads

## Crear Ambiente
### Clonar Repo
`git clone https://pgennoni@bitbucket.org/pgennoni/easy-git.git`

### Bajar el codigo de la rama principal del grupo
`git fetch && git checkout <nombre-rama>`

## Hacer brainstorming de frases, entre los participantes de cada equipo

## Subir cambios a rama local
`git add "<path-del-archivo>"`
`git commit -am "mensaje de commit"`
`git push -u`

## Merge y conflictos
Crear pull request, desde el dashboard de bitbucket

## Link del excel del taller
https://docs.google.com/spreadsheets/d/1PEonIkyBB7vSyibXhE4t3OjRCvs1tpRyFeqW2X8tZgw/edit?usp=sharing

## Mail de inicio de sesion en bitbucket.org por grupo
### Mails
equipoags2020@gmail.com
equipobgs2020@gmail.com
equipocgs2020@gmail.com
### Password
Galicia20   